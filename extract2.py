from direct.distributed.PyDatagram import PyDatagram
from direct.distributed.PyDatagramIterator import PyDatagramIterator

import os

with open("TTOffGame_decrypt.bin", 'rb') as opened_datagram:
    datagram = PyDatagram(opened_datagram.read())
    readable_datagram = PyDatagramIterator(datagram)

#warning = readable_datagram.getString()
totalModulesAmount = readable_datagram.getUint32() - 69

os.chdir('bytecode')

for module in range(totalModulesAmount):
    module_name = readable_datagram.getString()
    module_size = readable_datagram.getInt32() - 3
    module_code = readable_datagram.extractBytes(abs(module_size))

    if module_size < 0:
        module_name += '.__init__'

    with open(module_name + '.pyc', 'wb') as bytecode_file:
        magic = b'\x03\xF3\x0D\x0A' + b'\x00' * 4
        bytecode_file.write(magic + module_code)

    print(f'Extracted: {module_name}!')

print("All done!")
© 2020 GitHub, Inc.