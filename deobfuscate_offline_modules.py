from cStringIO import StringIO
from Crypto.Cipher import AES
import marshal, struct, types, glob, os, sys

def deobfuscate_method_code(code):
    chars = ['V', 'J', 'J', 'T', 'G', 'S', 'Q', 'C', 'D', 'H', 'I', 'S', 'T', 'U', 'I', 'Q', 'M', 'Y', 'T', 'I', 'C', 'M', 'Y', 'O', 'I', 'S', 'T', 'O', 'A', 'C', 'R', 'X']
    size = len(code)
    key = ['C', 'W', chars[size % 28], 'V', 'C', chars[size % 21], 'X', 'H', 'Y', 'Q', 'Y', 'P', chars[size % 10], 'O', 'O', 'U', 'T', 'X', chars[size % 16], 'W', 'K', 'N', 'J', chars[size % 3], 'G', 'W', 'I', 'J', 'E', chars[size % 7], 'E', 'A', chars[size % 5]]
    output = []

    for i in range(len(code)):
        xor_num = ord(code[i]) ^ ord(key[i % len(key)])
        output.append(chr(xor_num))

    output = output[::-1]

    return ''.join(output)

def deobfuscate_method_code_consts(x):
    size = len(x)
    newList = []

    for word in x:
        newString = []

        for i, letter in enumerate(word):
            newString.append(chr( (255 - ord(letter)) ^ ((2 * (i + 2)) % 255) ) )

        newList.append(''.join(newString))

    return tuple(newList)

def deobfuscate(x):
    print hex(ord(x[:1]))
    if not x.startswith('\x64'):
        code = deobfuscate_method_code(x)
        print ('Code deobfuscation!\n{}').format(code)
        return code
    else:
        return x

def dump(value, file):    
    #print(type(value))

    if isinstance(value, types.CodeType):
        dump_code(value, file)

    elif type(value) in (list, tuple):
      #  print 'list or tuple'
        file.write('[' if type(value) == list else '(')
        file.write(struct.pack('<I', len(value)))
        for x in value:
            dump(x, file)
            
    elif type(value) == dict:
        print('dict')
        file.write('{')
        for k, v in value.items():
            dump(k, file)
            dump(v, file)
        file.write('0')

    else:
        file.write(marshal.dumps(value))

def dump_code(value, file):
    file.write(struct.pack(
        '<cIIII', 'c', value.co_argcount, value.co_nlocals,
                       value.co_stacksize, value.co_flags))

    code = value.co_code
    print(code)
    dump(deobfuscate(code), file)
    dump(value.co_consts, file)
    consts = value.co_names
    dump(deobfuscate_method_code_consts(consts), file)
    dump(value.co_varnames, file)
    dump(value.co_freevars, file)
    dump(value.co_cellvars, file)
    dump(value.co_filename, file)
    dump(value.co_name, file)
    file.write(struct.pack('<I', value.co_firstlineno))
    dump(value.co_lnotab, file)

def dumps(value):
    sio = StringIO()
    dump(value, sio)
    return sio.getvalue()

if __name__ == '__main__':
    py27_header = '\x03\xF3\x0D\x0A\x00\x00\x00\x00'

    def get_deobfuscated_code(code):
        return dumps(code)

    if os.path.isdir(sys.argv[1]):
        bytecode_folder = glob.glob(('{}\\{}').format(sys.argv[1], '*.pyc'))
    elif os.path.isfile(sys.argv[1]):
        single_obj_list = [sys.argv[1]]
        bytecode_folder = single_obj_list
    for files in bytecode_folder:
        print ('Deobfuscating file: {}!').format(files)
        with open(files, 'rb') as (bytecode_files):
            bytecode_files.seek(8)
            game_code = bytecode_files.read()
            readable_game_code = marshal.loads(game_code)
            deobfuscation = get_deobfuscated_code(readable_game_code)
        with open(files, 'wb') as (writable_bytecode_files):
            writable_bytecode_files.write(py27_header + deobfuscation)

    linuxes = ['linux', 'linux2']
    if sys.platform in linuxes:
        if os.path.isdir(sys.argv[1]):
            bytecode_folder = glob.glob(sys.argv[1] + '/*.pyc')
        for files in bytecode_folder:
            print ('Deobfuscating file: {}!').format(files)
            with open(files, 'rb') as (bytecode_files):
                bytecode_files.seek(8)
                game_code = bytecode_files.read()
                readable_game_code = marshal.loads(game_code)
                deobfuscation = get_deobfuscated_code(readable_game_code)
            with open(files, 'wb') as (writable_bytecode_files):
                writable_bytecode_files.write(py27_header + deobfuscation)

    print('All done!')