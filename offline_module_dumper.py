from Crypto.Cipher import AES
from direct.distributed.PyDatagram import PyDatagram
from direct.distributed.PyDatagramIterator import PyDatagramIterator
import os, types, io, marshal

DUMP_DIR = 'dump'
MODULE_COUNT_OFFSET = 69

with open('ttoffengine_unpacked_0AC2B000.bin', 'rb') as code:
    code.seek(16 * 2)
    gamecode = code.read()

dg = PyDatagram(gamecode)
dgi = PyDatagramIterator(dg)

if not os.path.exists(DUMP_DIR):
    os.mkdir(DUMP_DIR)

os.chdir(DUMP_DIR)
PYTHONMAGIC = b'\x03\xF3\x0D\x0A' + b'\x00' * 4

num_modules = dgi.get_uint32() - MODULE_COUNT_OFFSET

# Dump the modules.
for i in range(num_modules):
    # Name of module.
    module = dgi.get_string()
    # Size of module.
    size = dgi.get_int32() - 3
    # Module data.
    data = dgi.extract_bytes(abs(size))

    # Negative sizes are inits.
    if size < 0:
        module = module + '.__init__'

    bytecodefilename = module + '.pyc'

    # Write the file to disk.
    with open(bytecodefilename, 'wb') as bytecode:
        bytecode.write(PYTHONMAGIC + data)

    print('Dumped module: {}!'.format(bytecodefilename))