from Crypto.Cipher import AES
import binascii

KEY = binascii.unhexlify(b'7953d9f61b585e21252a63029197091d40050b1c41bb2d6c7f8d1adaf61fc71e')
IV = binascii.unhexlify(b'ccba0e724b493eed6c446122f2a765e0413624812a3704a025b094f38b5e64a9')[:16]

aes = AES.new(KEY, AES.MODE_CBC, IV)

with open("TTOffGame.bin", 'rb') as game_bin:
    game_bin.seek(11)
    readable_game_bin = game_bin.read()

with open("TTOffGame_decrypt.bin", 'wb') as writable_bin:
    writable_bin.write(aes.decrypt(readable_game_bin))
